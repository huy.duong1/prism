﻿using Rikai.PRISM.WPF.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.UnitOfWork
{
    public interface IUnitOfWork
    {
        IMstHouseRepository MstHouseRepository { get; }
        IMstCourseRepository MstCourseRepository { get; }
        IMstMonthDateRepository MstMonthDateRepository { get; }
        IPrism330TableRepository Prism330TableRepository { get; }
        void SaveChange();
        void Rollback();
    }
}
