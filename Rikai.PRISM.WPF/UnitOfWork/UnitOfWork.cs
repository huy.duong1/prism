﻿using Rikai.PRISM.WPF.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private PRISMDB_TESTEntities _dbContext;

        public IMstHouseRepository MstHouseRepository { get; }
        public IMstCourseRepository MstCourseRepository { get; }
        public IMstMonthDateRepository MstMonthDateRepository { get; }

        public IPrism330TableRepository Prism330TableRepository { get; }

        public UnitOfWork(PRISMDB_TESTEntities dbContext,
                          IMstHouseRepository mstHouseRepository,
                          IMstCourseRepository mstCourseRepository,
                          IMstMonthDateRepository mstMonthDateRepository,
                          IPrism330TableRepository prism330TableRepository)
        {
            _dbContext = dbContext;
            MstHouseRepository = mstHouseRepository;
            MstCourseRepository = mstCourseRepository;
            MstMonthDateRepository = mstMonthDateRepository;
            Prism330TableRepository = prism330TableRepository;
        }


        public void Rollback()
        {
            _dbContext.Dispose();
        }

        public void SaveChange()
        {
            _dbContext.SaveChanges();
        }
    }
}
