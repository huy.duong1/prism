//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Rikai.PRISM.WPF
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwCourse
    {
        public short Year { get; set; }
        public short BrandCd { get; set; }
        public short CourseCd { get; set; }
        public string CourseName { get; set; }
        public Nullable<byte> CourseKbn { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public byte StdCourseCd { get; set; }
        public byte ClassSystem { get; set; }
        public Nullable<int> PriceGrade1 { get; set; }
        public Nullable<int> PriceGrade2 { get; set; }
        public Nullable<int> PriceGrade3 { get; set; }
        public Nullable<int> PriceGrade4 { get; set; }
        public Nullable<int> PriceGrade5 { get; set; }
        public Nullable<int> PriceGrade6 { get; set; }
        public Nullable<int> PriceGrade7 { get; set; }
        public Nullable<int> MaintenanceCost { get; set; }
        public Nullable<byte> MaintenanceCostKbn { get; set; }
        public Nullable<byte> CourseKikbn { get; set; }
    }
}
