﻿using Rikai.PRISM.WPF.Services.MstMonthDate.Dtos;
using Rikai.PRISM.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Commands
{
    public class UpdateMonthDateCommand : CommandBase
    {
        private PRISM330ViewModel _pRISM330ViewModel;
        
        public UpdateMonthDateCommand(PRISM330ViewModel pRISM330ViewModel)
        {
            _pRISM330ViewModel = pRISM330ViewModel;
        }
        public override void Execute(object parameter)
        {
            if (parameter is MstMonthDateResponseDto mstMonthDateResponseDto)
            {
                _pRISM330ViewModel.HandleUpdateMonthDateToView(mstMonthDateResponseDto);
            }
            
        }
    }
}
