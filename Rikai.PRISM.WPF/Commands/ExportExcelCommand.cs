﻿using Rikai.PRISM.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Commands
{
    public class ExportExcelCommand : CommandBase
    {
        private PRISM330ViewModel _pRISM330ViewModel;

        public ExportExcelCommand(PRISM330ViewModel pRISM330ViewModel)
        {
            _pRISM330ViewModel = pRISM330ViewModel;
        }
        public override void Execute(object parameter)
        {
            _pRISM330ViewModel.Export();
        }
    }
}
