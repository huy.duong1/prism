//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Rikai.PRISM.WPF
{
    using System;
    using System.Collections.Generic;
    
    public partial class mstFTPConnect
    {
        public string KeyCol { get; set; }
        public string SvrCol { get; set; }
        public string UsrCol { get; set; }
        public string PwdCol { get; set; }
        public string DirCol { get; set; }
    }
}
