﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Models
{
    public class Prism330TableModel
    {
        public int HouseCd { get; set; }
        public string HouseName { get; set; } // tên lớp học
        public string PupilCd { get; set; } // mã học sinh
        public string PupilLastName { get; set; } // tên học sinh
        public string PupilFirstName { get; set; } // họ học sinh
        public string PupilName { get; set; } // tên học sinh
        public byte Grade { get; set; } // khối
        public string Note { get; set; } // ghi chú -- cột 5
        public string SapixHouseName { get; set; } // tòa nhà
        public byte Sub1 { get; set; } 
        public byte Sub2 { get; set; } 
        public byte Sub3 { get; set; } 
        public byte Sub4 { get; set; } 
        public byte Sub5 { get; set; } 
        public byte Sub6 { get; set; } 
        public byte Sub7 { get; set; } 
        public byte Sub8 { get; set; } 
        public byte Sub9 { get; set; } 
        public byte Sub10 { get; set; } 
        public byte Sub11 { get; set; } 
        public byte Sub12 { get; set; } 
        public byte Sub13 { get; set; } 
        public byte TotalSubName { get; set; } 
    }
}
