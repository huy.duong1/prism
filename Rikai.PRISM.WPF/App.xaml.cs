﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Rikai.PRISM.WPF.Repositories;
using Rikai.PRISM.WPF.Repositories.Interface;
using Rikai.PRISM.WPF.Services.MstCourse;
using Rikai.PRISM.WPF.Services.MstHouse;
using Rikai.PRISM.WPF.Services.MstMonthDate;
using Rikai.PRISM.WPF.Services.Prism330Table;
using Rikai.PRISM.WPF.UnitOfWork;
using Rikai.PRISM.WPF.Utils.MapperHelpers;
using Rikai.PRISM.WPF.ViewModels;
using Rikai.PRISM.WPF.Views;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Rikai.PRISM.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static IHost AppHost { get; private set; }
        private readonly IServiceProvider _serviceProvider;

        public App()
        {

            AppHost = Host.CreateDefaultBuilder()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddScoped<PRISMDB_TESTEntities> ();

                    services.AddScoped<IMstHouseService, MstHouseService>()
                            .AddScoped<IMstCourseService, MstCourseService>()
                            .AddScoped<IMstMonthDateService, MstMonthDateService>()
                            .AddScoped<IPrism330TableRepository, Prism330TableRepository>()
                            .AddScoped<IUnitOfWork, Rikai.PRISM.WPF.UnitOfWork.UnitOfWork>()
                            .AddScoped<IMstHouseRepository, MstHouseRepository>()
                            .AddScoped<IMstCourseRepository, MstCourseRepository>()
                            .AddScoped<IMstMonthDateRepository, MstMonthDateRepository>()
                            .AddScoped<IPrism330TableService, Prism330TableService>()
                            .AddScoped<PRISM330ViewModel>();

                    // register mapper
                    services.AddAutoMapper(typeof(MstCourseMapper))
                            .AddAutoMapper(typeof(MstMonthDateMapper));
                })
                .Build();

            _serviceProvider = AppHost.Services;
        }
        protected override async void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            
            var prismWindow = new PRISM330View();
            var prism330 = _serviceProvider.GetService<PRISM330ViewModel>();
            prismWindow.DataContext = prism330;
            prismWindow.Show();

        }
    }
}
