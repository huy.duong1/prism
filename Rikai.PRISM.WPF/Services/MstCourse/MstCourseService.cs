﻿using AutoMapper;
using Rikai.PRISM.WPF.Services.MstCourse.Dtos;
using Rikai.PRISM.WPF.Services.MstHouse.Dtos;
using Rikai.PRISM.WPF.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Services.MstCourse
{
    public class MstCourseService : IMstCourseService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public MstCourseService(IUnitOfWork unitOfWork,
                                IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        
        public List<MstCourseSelectListDto> GetSelectList()
        {
            try
            {
                var currentYear = DateTime.Now.Year;
                var gLoginBrandCd = 6003;
                var mstCourse = _unitOfWork.MstCourseRepository.GetAll()
                                .Where(x => x.Year == currentYear && x.BrandCd == gLoginBrandCd)
                                .OrderByDescending(x => x.StartDate).ToList();
                
                var result = _mapper.Map<List<MstCourseSelectListDto>>(mstCourse);

                return result;
            }
            catch (Exception e)
            {
                // handle exception
                // save log
                return new List<MstCourseSelectListDto>();
            }
        }
    }
}
