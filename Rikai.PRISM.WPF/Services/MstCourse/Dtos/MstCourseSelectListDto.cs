﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Services.MstCourse.Dtos
{
    public class MstCourseSelectListDto
    {
        public short CourseCd { get; set; }
        public string CourseName { get; set; }
        public short BrandCd { get; set; }
        public Nullable<byte> CourseKbn { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }

        public string StartDateStr { get; set; }
        public string EndDateStr { get; set; }
    }
}
