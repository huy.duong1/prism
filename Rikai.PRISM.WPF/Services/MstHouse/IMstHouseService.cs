﻿using Rikai.PRISM.WPF.Services.MstHouse.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Services.MstHouse
{
    public interface IMstHouseService
    {
        List<MstHouseSelectListDto> GetSelectList();
    }
}
