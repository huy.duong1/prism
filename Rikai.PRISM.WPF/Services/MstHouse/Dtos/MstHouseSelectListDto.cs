﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Services.MstHouse.Dtos
{
    public class MstHouseSelectListDto
    {
        public int HouseCd { get; set; }
        public string HouseName { get; set; }
    }
}
