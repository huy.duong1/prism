﻿using Rikai.PRISM.WPF.Services.MstHouse.Dtos;
using Rikai.PRISM.WPF.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Services.MstHouse
{
    public class MstHouseService : IMstHouseService
    {
        private IUnitOfWork _unitOfWork;

        public MstHouseService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public List<MstHouseSelectListDto> GetSelectList()
        {
            try
            {
                var mstHouse = _unitOfWork.MstHouseRepository.GetAll().Select(x => new MstHouseSelectListDto
                {
                    HouseCd = x.HouseCd,
                    HouseName = x.HouseName

                }).ToList();

                return mstHouse;
            }
            catch (Exception e)
            {
                // handle exception
                // save log
                return new List<MstHouseSelectListDto>(); 
            }
        }
    }
}
