﻿using AutoMapper;
using Rikai.PRISM.WPF.Models;
using Rikai.PRISM.WPF.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Services.Prism330Table
{
    public class Prism330TableService : IPrism330TableService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public Prism330TableService(IUnitOfWork unitOfWork,
                                    IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public List<Prism330TableModel> GetPrism330TableModels(short courseCd, short brandCd, int houseCd, byte month)
        {
            try
            {
                var sqlQuery = @"SELECT
            TPD.*,
            MH.HouseCd,
            MH.HouseName,
            MH.BlockCd,
            MP.SapixHouseCd,
            MHS.SapixHouseName,
            CASE MP.StdKbn
                WHEN '2' THEN '○'
                ELSE ' '
            END AS StdKbn
        FROM
            tblPupilDraw AS TPD
            INNER JOIN v_mstPupil AS MP ON TPD.PupilCd = MP.PupilCd AND TPD.HouseCd = MP.HouseCd
            INNER JOIN vwHouseBlock AS MH ON MP.HouseCd = MH.HouseCd
            LEFT JOIN mstHouseSapix AS MHS ON MP.SapixHouseCd = MHS.SapixHouseCd
        WHERE
            TPD.CourseCd = @courseCd
            AND TPD.Month = @month
            AND TPD.BrandCd = @brandCd
            AND MP.BrandCd = @brandCd
            AND MH.HouseCd = @houseCd";

                var parameters = new[]
                               {
                                    new SqlParameter("@courseCd", courseCd),
                                    new SqlParameter("@month", month),
                                    new SqlParameter("@brandCd", brandCd),
                                    new SqlParameter("@houseCd", houseCd) };

                var result = _unitOfWork.Prism330TableRepository.SQLQuery(sqlQuery, parameters).ToList();

                return result;
            }
            catch(Exception e)
            {
                return new List<Prism330TableModel>();
            }
        }
    }
}
