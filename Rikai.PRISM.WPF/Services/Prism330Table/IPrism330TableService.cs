﻿using Rikai.PRISM.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Services.Prism330Table
{
    public interface IPrism330TableService
    {
        List<Prism330TableModel> GetPrism330TableModels(short courseCd, short brandCd, int houseCd, byte month);
    }
}
