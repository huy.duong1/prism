﻿using Rikai.PRISM.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Services.Prism330Table.Dtos
{
    public class Prism330TableListDto
    {
        public List<Prism330TableModel> Prism330TableModels;
        public byte TotalSub1 { get; set; }
        public byte TotalSub2 { get; set; }
        public byte TotalSub3 { get; set; }
        public byte TotalSub4 { get; set; }
        public byte TotalSub5 { get; set; }
        public byte TotalSub6 { get; set; }
        public byte TotalSub7 { get; set; }
        public byte TotalSub8 { get; set; }
        public byte TotalSub9 { get; set; }
        public byte TotalSub10 { get; set; }
        public byte TotalSub11 { get; set; }
        public byte TotalSub12 { get; set; }
        public byte TotalSub13 { get; set; }
        public byte TotalAmount { get; set; }

        public Prism330TableListDto(List<Prism330TableModel> prism330TableModels)
        {
            Prism330TableModels = prism330TableModels;
            
            TotalSub1 = (byte)(Prism330TableModels?.Sum(x => x.Sub1) ?? 0);
            TotalSub2 = (byte)(Prism330TableModels?.Sum(x => x.Sub2) ?? 0);
            TotalSub3 = (byte)(Prism330TableModels?.Sum(x => x.Sub3) ?? 0);
            TotalSub4 = (byte)(Prism330TableModels?.Sum(x => x.Sub4) ?? 0);
            TotalSub5 = (byte)(Prism330TableModels?.Sum(x => x.Sub5) ?? 0);
            TotalSub6 = (byte)(Prism330TableModels?.Sum(x => x.Sub6) ?? 0);
            TotalSub7 = (byte)(Prism330TableModels?.Sum(x => x.Sub7) ?? 0);
            TotalSub8 = (byte)(Prism330TableModels?.Sum(x => x.Sub8) ?? 0);
            TotalSub9 = (byte)(Prism330TableModels?.Sum(x => x.Sub9) ?? 0);
            TotalSub10 = (byte)(Prism330TableModels?.Sum(x => x.Sub10) ?? 0);
            TotalSub11 = (byte)(Prism330TableModels?.Sum(x => x.Sub11) ?? 0);
            TotalSub12 = (byte)(Prism330TableModels?.Sum(x => x.Sub12) ?? 0);
            TotalSub13 = (byte)(Prism330TableModels?.Sum(x => x.Sub13) ?? 0);
            TotalAmount = (byte)(Prism330TableModels?.Sum(x => x.TotalSubName) ?? 0);
        }
    }
}
