﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Services.MstMonthDate.Dtos
{
    public class MstMonthDateResponseDto
    {
        public short Year { get; set; }
        public short BrandCd { get; set; }
        public short CourseCd { get; set; }
        public byte Month { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string MonthView { get; set; }
        public string StartDateStr { get; set; }
        public string EndDateStr { get; set; }
    }
}
