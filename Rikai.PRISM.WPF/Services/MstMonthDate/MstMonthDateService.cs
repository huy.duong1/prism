﻿using AutoMapper;
using Rikai.PRISM.WPF.Services.MstMonthDate.Dtos;
using Rikai.PRISM.WPF.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Services.MstMonthDate
{
    public class MstMonthDateService : IMstMonthDateService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public MstMonthDateService(IUnitOfWork unitOfWork,
                                    IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        
        public List<MstMonthDateResponseDto> GetByMstCourse(short courseCd, short brandCd)
        {
            try
            {
                var mstMonthDates = _unitOfWork.MstMonthDateRepository.GetByMstCourse(courseCd, brandCd);
                var result = _mapper.Map<List<MstMonthDateResponseDto>>(mstMonthDates);

                return result;
            }
            catch(Exception e)
            {
                return new List<MstMonthDateResponseDto>();
            }
        }
    }
}
