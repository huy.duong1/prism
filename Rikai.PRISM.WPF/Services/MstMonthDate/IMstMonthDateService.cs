﻿using Rikai.PRISM.WPF.Services.MstMonthDate.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Services.MstMonthDate
{
    public interface IMstMonthDateService
    {
        List<MstMonthDateResponseDto> GetByMstCourse(short courseCd, short brandCd);
    }
}
