//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Rikai.PRISM.WPF
{
    using System;
    using System.Collections.Generic;
    
    public partial class mstBirthYear
    {
        public short Year { get; set; }
        public short BrandCd { get; set; }
        public int Grade { get; set; }
        public short BirthYear { get; set; }
    }
}
