﻿using OfficeOpenXml;
using Rikai.PRISM.WPF.Commands;
using Rikai.PRISM.WPF.Dialogs;
using Rikai.PRISM.WPF.Models;
using Rikai.PRISM.WPF.Services.MstCourse;
using Rikai.PRISM.WPF.Services.MstCourse.Dtos;
using Rikai.PRISM.WPF.Services.MstHouse;
using Rikai.PRISM.WPF.Services.MstHouse.Dtos;
using Rikai.PRISM.WPF.Services.MstMonthDate;
using Rikai.PRISM.WPF.Services.MstMonthDate.Dtos;
using Rikai.PRISM.WPF.Services.Prism330Table;
using Rikai.PRISM.WPF.Services.Prism330Table.Dtos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace Rikai.PRISM.WPF.ViewModels
{
    public class PRISM330ViewModel : INotifyPropertyChanged
    {
        private IMstHouseService _mstHouseService;
        private IMstCourseService _mstCourseService;
        private IMstMonthDateService _mstMonthDateService;
        private IPrism330TableService _prism330TableService;

        public PRISM330ViewModel(IMstHouseService mstHouseService,
                                IMstCourseService mstCourseService,
                                IMstMonthDateService mstMonthDateService,
                                IPrism330TableService prism330TableService)
        {
            _mstHouseService = mstHouseService;
            _mstCourseService = mstCourseService;
            _mstMonthDateService = mstMonthDateService;
            _prism330TableService = prism330TableService;
            InitialData();

            // handle command
            UpdateMonthDateCommand = new UpdateMonthDateCommand(this);
            SumMonthDateCommand = new SumMonthDateCommand(this);
            ExportCommand = new ExportExcelCommand(this);
        }

        private void InitialData()
        {
            var mstHouseSelectListDtos = _mstHouseService.GetSelectList();
            var mstCourseSelectListDto = _mstCourseService.GetSelectList();

            MstHouseSelectListDto = new ObservableCollection<MstHouseSelectListDto>(mstHouseSelectListDtos);
            MstCourseSelectListDto = new ObservableCollection<MstCourseSelectListDto>(mstCourseSelectListDto);
            ChangeSelectMstHouseSelectListDto(MstHouseSelectListDto);
            ChangeSelectMstCourseSelectListDto(MstCourseSelectListDto);
        }


        #region binding-data
        private void ChangeSelectMstHouseSelectListDto(ObservableCollection<MstHouseSelectListDto> MstHouseSelectListDto)
        {
            if (MstHouseSelectListDto != null && MstHouseSelectListDto.Count > 0)
            {
                SelectMstHouseSelectListDto = MstHouseSelectListDto[0];
            }
        }

        private void ChangeSelectMstCourseSelectListDto(ObservableCollection<MstCourseSelectListDto> MstCourseSelectListDto)
        {
            if (MstCourseSelectListDto != null && MstCourseSelectListDto.Count > 0)
            {
                SelectMstCourseSelectListDto = MstCourseSelectListDto[0];
            }
        }


        private ObservableCollection<MstHouseSelectListDto> _mstHouseSelectListDtos;
        public ObservableCollection<MstHouseSelectListDto> MstHouseSelectListDto
        {
            get { return _mstHouseSelectListDtos; }
            set
            {
                _mstHouseSelectListDtos = value;
                OnPropertyChanged(nameof(MstHouseSelectListDto));
            }
        }
        private MstHouseSelectListDto _selectMstHouseSelectListDto;
        public MstHouseSelectListDto SelectMstHouseSelectListDto
        {
            get { return _selectMstHouseSelectListDto; }
            set
            {
                _selectMstHouseSelectListDto = value;
                OnPropertyChanged(nameof(SelectMstHouseSelectListDto));
            }
        }

        private ObservableCollection<MstCourseSelectListDto> _mstCourseSelectListDto;
        public ObservableCollection<MstCourseSelectListDto> MstCourseSelectListDto
        {
            get { return _mstCourseSelectListDto; }
            set
            {
                _mstCourseSelectListDto = value;
                OnPropertyChanged(nameof(MstCourseSelectListDto));
            }
        }

        private MstCourseSelectListDto _selectMstCourseSelectListDto;
        public MstCourseSelectListDto SelectMstCourseSelectListDto
        {
            get { return _selectMstCourseSelectListDto; }
            set
            {
                _selectMstCourseSelectListDto = value;
                IsBlockVisible = (value != null && value.CourseKbn == 1); // visible = true => view
                InitialDataLViewMonthDate(value.CourseCd, value.BrandCd, value.CourseKbn ?? 0);
                OnPropertyChanged(nameof(SelectMstCourseSelectListDto));
            }
        }
        private bool _isBlockVisible;
        public bool IsBlockVisible
        {
            get { return _isBlockVisible; }
            set
            {
                _isBlockVisible = value;
                OnPropertyChanged(nameof(IsBlockVisible));
            }
        }

        private ObservableCollection<MstMonthDateResponseDto> _mstMonthDateResponseDtos;
        public ObservableCollection<MstMonthDateResponseDto> MstMonthDateResponseDtos
        {
            get { return _mstMonthDateResponseDtos; }
            set
            {
                _mstMonthDateResponseDtos = value;
                OnPropertyChanged(nameof(MstMonthDateResponseDtos));
            }
        }

        private MstMonthDateResponseDto _selectMstMonthDateResponseDto;
        public MstMonthDateResponseDto SelectMstMonthDateResponseDto
        {
            get { return _selectMstMonthDateResponseDto; }
            set
            {
                _selectMstMonthDateResponseDto = value;
                OnPropertyChanged(nameof(SelectMstMonthDateResponseDto));
            }
        }
        private MstMonthDateResponseDto _mstMonthDateTextBlock;
        public MstMonthDateResponseDto MstMonthDateTextBlock
        {
            get { return _mstMonthDateTextBlock; }
            set
            {
                _mstMonthDateTextBlock = value;
                OnPropertyChanged(nameof(MstMonthDateTextBlock));

            }
        }
        private ObservableCollection<Prism330TableModel> _prism330TableModels;
        public ObservableCollection<Prism330TableModel> Prism330TableModels
        {
            get { return _prism330TableModels; }
            set
            {
                _prism330TableModels = value;
                OnPropertyChanged(nameof(Prism330TableModels));
            }
        }
        private Prism330TableListDto _prism330TableListDtos;
        public Prism330TableListDto Prism330TableListDtos
        {
            get { return _prism330TableListDtos; }
            set
            {
                _prism330TableListDtos = value;
                OnPropertyChanged(nameof(Prism330TableListDtos));
            }
        }
        #endregion

        private void InitialDataLViewMonthDate(short courseCd, short brandCd, byte courseKbn)
        {
            try
            {
                if (courseKbn == 1)
                {
                    var data = _mstMonthDateService.GetByMstCourse(courseCd, brandCd);
                    MstMonthDateResponseDtos = new ObservableCollection<MstMonthDateResponseDto>(data);
                }

            }
            catch (Exception e)
            {

            }
        }
        #region commnad
        public ICommand UpdateMonthDateCommand { get; }
        public ICommand SumMonthDateCommand { get; }
        public ICommand ExportCommand { get; set; }
        #endregion

        public void HandleUpdateMonthDateToView(MstMonthDateResponseDto param)
        {
            MstMonthDateTextBlock = param;
        }

        public void HandleSumMonthDateToView()
        {
            if (SelectMstCourseSelectListDto.CourseKbn == 2)
            {
                ShowNotification();
                Prism330TableModels = new ObservableCollection<Prism330TableModel>();
                Prism330TableListDtos = null;
            }
            else if (MstMonthDateTextBlock == null || MstMonthDateTextBlock.Month == 0)
            {
                ShowNotification();
                Prism330TableModels = new ObservableCollection<Prism330TableModel>();
                Prism330TableListDtos = null;

            }
            else
            {
                var prism330TableModels = _prism330TableService.GetPrism330TableModels(SelectMstCourseSelectListDto.CourseCd, SelectMstCourseSelectListDto.BrandCd, SelectMstHouseSelectListDto.HouseCd, MstMonthDateTextBlock.Month);
                Prism330TableModels = new ObservableCollection<Prism330TableModel>(prism330TableModels);
                Prism330TableListDtos = new Prism330TableListDto(prism330TableModels);
            }
        }
        public void Export()
        {
            if (Prism330TableModels == null || Prism330TableModels.Count == 0)
            {
                ShowNotification();
                return;
            }
            else
            {
                DialogResult result = System.Windows.Forms.MessageBox.Show("Xác nhận export file Excel?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    // Hiển thị hộp thoại chọn thư mục để lưu file Excel
                    using (var folderDialog = new FolderBrowserDialog())
                    {
                        if (folderDialog.ShowDialog() == DialogResult.OK)
                        {
                            string folderPath = folderDialog.SelectedPath;


                            // Export file Excel
                            ExportToExcel(folderPath);
                        }
                    }
                }
            }
           
        }

        private void ExportToExcel(string folderPath)
        {
            // Tạo một package Excel
            using (var package = new ExcelPackage())
            {
                // Tạo một worksheet mới
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Sheet1");

                // Thêm header
                worksheet.Cells["A1"].Value = "教室";
                worksheet.Cells["B1"].Value = "生徒CD";
                worksheet.Cells["C1"].Value = "生徒名";
                worksheet.Cells["D1"].Value = "学年";
                worksheet.Cells["E1"].Value = "講";
                worksheet.Cells["F1"].Value = "SAPIX校舎";
                worksheet.Cells["G1"].Value = "算数";
                worksheet.Cells["H1"].Value = "国語";
                worksheet.Cells["I1"].Value = "理科";
                worksheet.Cells["J1"].Value = "社会";
                worksheet.Cells["K1"].Value = "数学";
                worksheet.Cells["L1"].Value = "英語";
                worksheet.Cells["M1"].Value = "サン";
                worksheet.Cells["N1"].Value = "コク";
                worksheet.Cells["O1"].Value = "リカ";
                worksheet.Cells["P1"].Value = "シャ";
                worksheet.Cells["Q1"].Value = "追加";
                worksheet.Cells["R1"].Value = "領収済";
                worksheet.Cells["S1"].Value = "調整";
                worksheet.Cells["T1"].Value = "合計";

                // Điền dữ liệu vào worksheet
                int rowIndex = 2; // Bắt đầu từ hàng thứ 2
                foreach (var data in Prism330TableModels)
                {
                    worksheet.Cells["A" + rowIndex].Value = data.HouseName;
                    worksheet.Cells["B" + rowIndex].Value = data.PupilCd;
                    worksheet.Cells["C" + rowIndex].Value = data.PupilName;
                    worksheet.Cells["D" + rowIndex].Value = data.Grade;
                    worksheet.Cells["E" + rowIndex].Value = data.Note;
                    worksheet.Cells["F" + rowIndex].Value = data.SapixHouseName;
                    worksheet.Cells["G" + rowIndex].Value = data.Sub1;
                    worksheet.Cells["H" + rowIndex].Value = data.Sub2;
                    worksheet.Cells["I" + rowIndex].Value = data.Sub3;
                    worksheet.Cells["J" + rowIndex].Value = data.Sub4;
                    worksheet.Cells["K" + rowIndex].Value = data.Sub5;
                    worksheet.Cells["L" + rowIndex].Value = data.Sub6;
                    worksheet.Cells["M" + rowIndex].Value = data.Sub7;
                    worksheet.Cells["N" + rowIndex].Value = data.Sub8;
                    worksheet.Cells["O" + rowIndex].Value = data.Sub9;
                    worksheet.Cells["P" + rowIndex].Value = data.Sub10;
                    worksheet.Cells["Q" + rowIndex].Value = data.Sub11;
                    worksheet.Cells["R" + rowIndex].Value = data.Sub12;
                    worksheet.Cells["S" + rowIndex].Value = data.Sub13;
                    worksheet.Cells["T" + rowIndex].Value = data.TotalSubName;

                    rowIndex++;
                }

                // Lưu file Excel vào thư mục chỉ định
                string filePath = Path.Combine(folderPath, "exported_file.xlsx");
                package.SaveAs(new FileInfo(filePath));
            }

            System.Windows.Forms.MessageBox.Show("File Excel đã được xuất thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void ShowNotification()
        {
            var viewModel = new NotificationViewModel();
            viewModel.Title = "Notification";
            viewModel.Message = "対象月を入力してください。";

            var window = new NotificationDialog();
            window.DataContext = viewModel;

            // Đăng ký sự kiện Closed của NotificationViewModel
            viewModel.Closed += (sender, e) =>
            {
                // Đóng cửa sổ thông báo
                window.Close();
            };

            window.ShowDialog();
        }
        #region implement INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
