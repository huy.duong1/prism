﻿using Rikai.PRISM.WPF.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Rikai.PRISM.WPF.ViewModels
{
    public class NotificationViewModel : INotifyPropertyChanged
    {
        private string _title;
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged(nameof(Title));
            }
        }

        private string _message;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }
        public event EventHandler Closed;
        public ICommand CloseCommand { get; }

        public NotificationViewModel()
        {
            CloseCommand = new RelayCommand(param => Close());
        }

        private void Close()
        {


            // Kích hoạt sự kiện Closed để thông báo rằng thông báo đã được đóng
            Closed?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        // Implement INotifyPropertyChanged (nếu chưa có)
    }
}
