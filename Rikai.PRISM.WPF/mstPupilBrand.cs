//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Rikai.PRISM.WPF
{
    using System;
    using System.Collections.Generic;
    
    public partial class mstPupilBrand
    {
        public string PupilCd { get; set; }
        public short BrandCd { get; set; }
        public int HouseCd { get; set; }
        public Nullable<byte> StdCourseCd { get; set; }
        public Nullable<byte> ClassSystem { get; set; }
        public Nullable<byte> StdKbn { get; set; }
        public Nullable<int> MainFlg { get; set; }
        public Nullable<byte> DelFlg { get; set; }
    }
}
