﻿using AutoMapper;
using Rikai.PRISM.WPF.Models;
using Rikai.PRISM.WPF.Services.MstMonthDate.Dtos;
using Rikai.PRISM.WPF.Utils.GlobalHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Utils.MapperHelpers
{
    public class MstMonthDateMapper : Profile
    {
        public MstMonthDateMapper()
        {
            CreateMap<mstMonthDate, MstMonthDateResponseDto>()
                  .ForMember(d => d.EndDateStr, o => o.MapFrom(src => src.EndDate.HasValue ? src.EndDate.Value.ToVietNamFormatDate() : ""))
                  .ForMember(d => d.StartDateStr, o => o.MapFrom(src => src.StartDate.HasValue ? src.StartDate.Value.ToVietNamFormatDate() : ""))
                  .ReverseMap();

            CreateMap<MstMonthDateModel, MstMonthDateResponseDto>()
                 .ForMember(d => d.EndDateStr, o => o.MapFrom(src => src.EndDate.HasValue ? src.EndDate.Value.ToVietNamFormatDate() : ""))
                 .ForMember(d => d.StartDateStr, o => o.MapFrom(src => src.StartDate.HasValue ? src.StartDate.Value.ToVietNamFormatDate() : ""))
                 .ReverseMap();
        }
    }
}
