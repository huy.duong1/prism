﻿using AutoMapper;
using Rikai.PRISM.WPF.Services.MstCourse.Dtos;
using Rikai.PRISM.WPF.Utils.GlobalHelpers;

namespace Rikai.PRISM.WPF.Utils.MapperHelpers
{
    public class MstCourseMapper : Profile
    {
        public MstCourseMapper()
        {
            CreateMap<mstCourse, MstCourseSelectListDto>()
                .ForMember(d => d.EndDateStr, o => o.MapFrom(src => src.EndDate.HasValue ? src.EndDate.Value.ToVietNamFormatDate() : ""))
                .ForMember(d => d.StartDateStr, o => o.MapFrom(src => src.StartDate.HasValue ? src.StartDate.Value.ToVietNamFormatDate() : ""))
                .ReverseMap();
        }
    }
}
