﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Utils.GlobalHelpers
{
    public static class DateHelper
    {
        public static string ToVietNamFormat(this DateTime dateTime)
        {
            return dateTime.ToString("dd/MM/yyyy HH:mm:ss");
        }
        public static string ToVietNamFormatDate(this DateTime dateTime)
        {
            return dateTime.ToString("dd/MM/yyyy");
        }

        public static DateTime ConvertToDateTime(string value)
        {
            DateTime result;
            DateTime.TryParseExact(value, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out result);
            return result;
        }

        public static bool IsValidateTime(string value)
        {
            DateTime result;
            return DateTime.TryParseExact(value, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out result);
            {
            }
        }
        public static string GetTimeAgoString(DateTime dateTime)
        {
            // TimeSpan: đối tượng để tính toán khoảng thời gian
            TimeSpan timeAgo = DateTime.Now - dateTime;

            if (timeAgo.TotalSeconds < 60)
            {
                return $"{Math.Floor(timeAgo.TotalSeconds)} giây trước";
            }
            else if (timeAgo.TotalMinutes < 60)
            {
                return $"{Math.Floor(timeAgo.TotalMinutes)} phút trước";
            }
            else if (timeAgo.TotalHours < 24)
            {
                return $"{Math.Floor(timeAgo.TotalHours)} giờ trước";
            }
            else if (timeAgo.TotalDays < 7)
            {
                return $"{Math.Floor(timeAgo.TotalDays)} ngày trước";
            }
            else if (timeAgo.TotalDays < 30)
            {
                int weeks = (int)Math.Floor(timeAgo.TotalDays / 7);
                return $"{weeks} tuần trước";
            }
            else if (timeAgo.TotalDays < 365)
            {
                int months = (int)Math.Floor(timeAgo.TotalDays / 30);
                return $"{months} tháng trước";
            }
            else
            {
                int years = (int)Math.Floor(timeAgo.TotalDays / 365);
                return $"{years} năm trước";
            }
        }
    }
}
