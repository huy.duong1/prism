﻿using Rikai.PRISM.WPF.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Repositories
{
    public class MstHouseRepository : Repository<mstHouse>, IMstHouseRepository
    {
        public MstHouseRepository(PRISMDB_TESTEntities dbContext) : base(dbContext)
        {
        }
    }
}
