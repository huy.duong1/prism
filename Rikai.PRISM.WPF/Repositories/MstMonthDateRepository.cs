﻿using Rikai.PRISM.WPF.Models;
using Rikai.PRISM.WPF.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Repositories
{
    public class MstMonthDateRepository : Repository<mstMonthDate>, IMstMonthDateRepository
    {
        private PRISMDB_TESTEntities _dbContext;
        public MstMonthDateRepository(PRISMDB_TESTEntities context) : base(context)
        {
            _dbContext = context;
        }

        public IEnumerable<MstMonthDateModel> GetByMstCourse(short courseCd, short brandCd)
        {
            var currentYear = DateTime.Now.Year;
            var query = @"SELECT
                    Month,
                    CASE
                        WHEN ISNULL(Month, 0) > 0 THEN CAST(ISNULL(Month, 0) AS VARCHAR) + '月'
                        ELSE ''
                    END AS MonthView,
                    StartDate,
                    EndDate
                FROM
                    mstMonthDate
                WHERE
                    CourseCd = @courseCd
                    AND [Year] = @currentYear
                    AND BrandCd = @brandCd
                ORDER BY
                    StartDate";

            var parameters = new[]
                                {
                                    new SqlParameter("@courseCd", courseCd),
                                    new SqlParameter("@currentYear", currentYear),
                                    new SqlParameter("@brandCd", brandCd)
                                };

            return _dbContext.Database.SqlQuery<MstMonthDateModel>(query, parameters).ToList();
        }
    }
}
