﻿using Rikai.PRISM.WPF.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Repositories
{
    public class MstCourseRepository : Repository<mstCourse>, IMstCourseRepository
    {
        public MstCourseRepository(PRISMDB_TESTEntities dbContext) : base(dbContext)
        {
        }
    }
}
