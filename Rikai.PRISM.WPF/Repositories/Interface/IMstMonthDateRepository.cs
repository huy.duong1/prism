﻿using Rikai.PRISM.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Repositories.Interface
{
    public interface IMstMonthDateRepository : IRepository<mstMonthDate>
    {
        IEnumerable<MstMonthDateModel> GetByMstCourse(short courseCd, short brandCd);
    }
}
