﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Repositories.Interface
{
    public interface IRepository<T> where T : class
    {
        ICollection<T> GetAll();
        T GetBy(Expression<Func<T, bool>> match);
        T Add(T entity);
        T Update(T entity, object key);
        List<T> Pagination(
           out int total,
           int pageNumber,
           int pageSize,
           Expression<Func<T, bool>> predicate = null,
           Expression<Func<T, T>> selector = null,
           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null
       );
        IEnumerable<T> SQLQuery(string sql, params object[] parameters);
        Task<IEnumerable<T>> SQLQueryAsync(string sql, params object[] parameters);
    }
}
