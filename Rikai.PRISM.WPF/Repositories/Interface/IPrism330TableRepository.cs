﻿using Rikai.PRISM.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Repositories.Interface
{
    public interface IPrism330TableRepository : IRepository<Prism330TableModel>
    {
    }
}
