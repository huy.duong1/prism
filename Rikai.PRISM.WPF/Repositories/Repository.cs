﻿using Rikai.PRISM.WPF.Models;
using Rikai.PRISM.WPF.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace Rikai.PRISM.WPF.Repositories
{
    public class Repository<T> : IRepository<T> where T : class, new()
    {
        private PRISMDB_TESTEntities _dbContext;
        private DbSet<T> _dbSet;

        public Repository(PRISMDB_TESTEntities dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<T>();
        }
        public T Add(T entity)
        {
            _dbSet.Add(entity);
            return entity;
        }

        public ICollection<T> GetAll()
        {
            var data = _dbSet.ToList();
            return data;
        }

        public T GetBy(Expression<Func<T, bool>> match)
        {
            var data = _dbSet.SingleOrDefault(match);
            return data;

        }

        public List<T> Pagination(out int total, int pageNumber, int pageSize, Expression<Func<T, bool>> predicate = null, Expression<Func<T, T>> selector = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null)
        {
            IQueryable<T> query = _dbSet;



            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            total = query.Count();

            query = query.Skip((pageNumber - 1) * pageSize)
                         .Take(pageSize);

            if (selector != null)
            {
                query = query.Select(selector);
            }

            return query.ToList();
        }

        public T Update(T entity, object key)
        {
            if (entity == null)
                return null;
            T exist = _dbSet.Find(key);
            if (exist != null)
            {
                _dbContext.Entry(exist).CurrentValues.SetValues(entity);
            }
            return exist;
        }
        public IEnumerable<T> SQLQuery(string sql, params object[] parameters)
        {
            var data= _dbContext.Database.SqlQuery<T>(sql, parameters).ToList();
            return data;
        }

        public async Task<IEnumerable<T>> SQLQueryAsync(string sql, params object[] parameters)
        {
            var data = await _dbContext.Database.SqlQuery<T>(sql, parameters).ToListAsync();
            return data;
        }
    }
}
