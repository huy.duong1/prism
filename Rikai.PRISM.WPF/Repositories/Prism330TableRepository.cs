﻿using Rikai.PRISM.WPF.Models;
using Rikai.PRISM.WPF.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rikai.PRISM.WPF.Repositories
{
    public class Prism330TableRepository : Repository<Prism330TableModel>, IPrism330TableRepository
    {
        public Prism330TableRepository(PRISMDB_TESTEntities dbContext) : base(dbContext)
        {
        }
    }
}
