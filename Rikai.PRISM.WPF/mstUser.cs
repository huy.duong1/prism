//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Rikai.PRISM.WPF
{
    using System;
    using System.Collections.Generic;
    
    public partial class mstUser
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Authority { get; set; }
        public int HouseCd { get; set; }
        public string BlockCd { get; set; }
        public string FLGJ { get; set; }
        public string FLGS { get; set; }
        public Nullable<System.DateTime> UpdStampDate { get; set; }
        public string UpdStampId { get; set; }
        public Nullable<System.DateTime> InsStampDate { get; set; }
        public string InsStampId { get; set; }
    }
}
