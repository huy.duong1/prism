//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Rikai.PRISM.WPF
{
    using System;
    using System.Collections.Generic;
    
    public partial class mstStdPass
    {
        public string PupilCd { get; set; }
        public string Password { get; set; }
        public string OldPassword { get; set; }
        public Nullable<System.DateTime> UpdStampDate { get; set; }
        public string UpdStampId { get; set; }
        public Nullable<System.DateTime> InsStampDate { get; set; }
        public string InsStampId { get; set; }
        public Nullable<byte> DelFlg { get; set; }
        public string TmpPassword { get; set; }
        public string PasswordKana { get; set; }
        public string Password2 { get; set; }
        public string Password2Kana { get; set; }
    }
}
