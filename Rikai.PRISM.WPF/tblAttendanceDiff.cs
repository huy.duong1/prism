//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Rikai.PRISM.WPF
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblAttendanceDiff
    {
        public int HouseCd { get; set; }
        public int TeacherCd { get; set; }
        public Nullable<int> TeacherNo { get; set; }
        public System.DateTime AsOfDate { get; set; }
        public string TeacherName { get; set; }
        public Nullable<byte> TeacherKbn { get; set; }
        public Nullable<byte> KomaFull { get; set; }
        public Nullable<byte> Koma11 { get; set; }
        public Nullable<byte> Koma12 { get; set; }
        public Nullable<byte> KomaJH11 { get; set; }
        public Nullable<byte> KomaJH12 { get; set; }
        public Nullable<byte> KomaPAST { get; set; }
        public Nullable<System.DateTime> PreWork { get; set; }
        public Nullable<System.DateTime> Work { get; set; }
        public string Adjust { get; set; }
        public Nullable<short> ExpenseId { get; set; }
        public Nullable<int> Expense { get; set; }
        public string ChgCols { get; set; }
        public Nullable<System.DateTime> UpdStampDate { get; set; }
        public string UpdStampId { get; set; }
        public Nullable<System.DateTime> InsStampDate { get; set; }
        public string InsStampId { get; set; }
    }
}
