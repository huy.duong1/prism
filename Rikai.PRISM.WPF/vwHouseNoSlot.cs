//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Rikai.PRISM.WPF
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwHouseNoSlot
    {
        public short Year { get; set; }
        public short BrandCd { get; set; }
        public int HouseCd { get; set; }
        public short CourseCd { get; set; }
        public byte Week { get; set; }
        public Nullable<int> Sign01 { get; set; }
        public Nullable<int> Sign02 { get; set; }
        public Nullable<int> Sign03 { get; set; }
        public Nullable<int> Sign04 { get; set; }
        public Nullable<int> Sign05 { get; set; }
        public Nullable<int> Sign06 { get; set; }
        public Nullable<int> Sign07 { get; set; }
        public Nullable<int> Sign08 { get; set; }
        public Nullable<int> Sign09 { get; set; }
        public Nullable<int> Sign10 { get; set; }
        public Nullable<int> Sign11 { get; set; }
    }
}
