USE [PRISMDB_TEST]
Go
CREATE USER [aps] FOR LOGIN [aps] WITH DEFAULT_SCHEMA=[dbo]
Go
ALTER ROLE [db_datareader] ADD MEMBER [aps]
Go
ALTER ROLE [db_datawriter] ADD MEMBER [aps]
Go


select SubjectCd, SubjectName  from mstSubject where SubjectCd between 1 and 10 
select *  from mstSubject where SubjectCd between 1 and 10 

select * from   tblActStandard where CourseCd =1
select * from mstStudent where PupilCd = '390000826'
select * from tblActStandard
select * from mstPupil
select * from tblPupilAdjust where PupilCd = '390000585'
select * from v_mstPupil where PupilCd = '390000585'
select * from mstHouse
select * from mstMonthDate where CourseCd =1
select * from mstCourse where Year = 2023 and BrandCd = 6003
order by StartDate

SELECT
  Month,
  CASE
    WHEN ISNULL(Month, 0) > 0 THEN CAST(ISNULL(Month, 0) AS VARCHAR) + '��'
    ELSE ''
  END AS MonthView,
  StartDate,
  EndDate
FROM
  mstMonthDate
WHERE
  CourseCd = '2' 
  AND [Year] = 2023
  AND BrandCd = 6003
ORDER BY
  StartDate



-- L?y d? li?u t? b?ng mstSubject
SELECT SubjectCd, SubjectName
FROM mstSubject
WHERE SubjectCd BETWEEN 1 AND 10
  AND BrandCd = 2;


-- L?y d? li?u t? b?ng mstHouse
-- Tr??ng h?p gMenuKbn = 0
SELECT HouseCd, HouseName
FROM mstHouse
-- WHERE HouseCd != 0
WHERE HouseCd <> 1
ORDER BY HouseCd;


-- Tr??ng h?p gMenuKbn = 1
SELECT {COMBO_KEY_ALL_SCHOOL} AS HouseCd, '{COMBO_STRING_ALL_SCHOOL}' AS HouseName
UNION
SELECT HO.HouseCd AS HouseCd, HO.HouseName AS HouseName
FROM mstBlockHouse BH
INNER JOIN mstHouse AS HO ON BH.HouseCd = HO.HouseCd
WHERE BH.BlockCd = '{gBlockCd}'
ORDER BY HouseCd;

-- Tr??ng h?p gMenuKbn = 2
SELECT {COMBO_KEY_ALL_SCHOOL} AS HouseCd, '{COMBO_STRING_ALL_SCHOOL}' AS HouseName
UNION
SELECT HouseCd, HouseName
FROM mstHouse
WHERE HouseCd <> {HOUSE_CD_MASTER}
ORDER BY HouseCd;

-- l?y d? li?u kh?a h?c

SELECT CourseCd, CourseName, StartDate, EndDate, CourseKbn
FROM mstCourse AS MC
WHERE Year = @CurrentYear AND BrandCd = @LoginBrandCd
ORDER BY StartDate DESC


SELECT
    TAS.SubjectCd1,
	TAS.SubjectCd2,
	TAS.SubjectCd3,
	TAS.SubjectCd4,
	TAS.SubjectCd5,
	TAS.SubjectCd6,
	TAS.SubjectCd7,
	TAS.SubjectCd8,
	TAS.SubjectCd9,
	TAS.SubjectCd10,
	TAS.SubjectCd11,
	TAS.SubjectCd12,
	TAS.SubjectCd13,
    MH.HouseCd,
    MH.HouseName,
    MH.BlockCd
FROM
    tblActStandard AS TAS
    INNER JOIN mstTeacher AS MT ON TAS.TeacherCd = MT.TeacherCd
    INNER JOIN vwHouseBlock AS MH ON TAS.HouseCd = MH.HouseCd
    INNER JOIN tblTeacherHouse AS TTH ON TAS.HouseCd = TTH.HouseCd AND TAS.TeacherCd = TTH.TeacherCd
WHERE
    TAS.AsOfDate BETWEEN '2022-03-05' AND '2022-02-28'
    and TAS.CourseCd = 1
    AND TAS.Year = 2023
    AND TAS.BrandCd = 6003
	------
	SELECT
  TPD.*,
  MH.HouseCd,
  MH.HouseName,
  MH.BlockCd,
  MP.SapixHouseCd,
  MHS.SapixHouseName,
  CASE MP.StdKbn
    WHEN '2' THEN '��'
    ELSE ' '
  END AS StdKbn
FROM
  tblPupilDraw AS TPD
  INNER JOIN v_mstPupil AS MP ON TPD.PupilCd = MP.PupilCd AND TPD.HouseCd = MP.HouseCd
  INNER JOIN vwHouseBlock AS MH ON MP.HouseCd = MH.HouseCd
  LEFT JOIN mstHouseSapix AS MHS ON MP.SapixHouseCd = MHS.SapixHouseCd
  --where MP.PupilCd = '390000826'

WHERE
  TPD.CourseCd = 1
  AND TPD.Month = 3
  --AND TPD.Year = 2022
  AND TPD.BrandCd = 6003
  AND MP.BrandCd = 6003
  And MH.HouseCd = 7

  ----

  SELECT
  ISNULL(MP.SapixHouseCd, 0) AS SapixHouseCd,
  MHS.SapixHouseName,
  CASE MP.StdKbn
    WHEN '2' THEN '��'
    ELSE ' '
  END AS StdKbn
FROM
  mstPupil AS MP
  LEFT JOIN mstHouseSapix AS MHS ON MP.SapixHouseCd = MHS.SapixHouseCd
WHERE
  MP.PupilCd = '390000826'
  AND MP.BrandCd = 6063


